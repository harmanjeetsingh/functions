const getSum = (str1, str2) => {
  if(str1 instanceof Object || Array.isArray(str1) || str2 instanceof Object || Array.isArray(str2))
    return false
  else if((str1.match(/[a-z]/i)) || (str2.match(/[a-z]/i))){
    return false;
  }
  else{
    let string1 = Number(str1)
    let string2 = Number(str2)
    let resultantString = string1+string2
    return String(resultantString)
  }
};



const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countposts = 0, countcomments = 0;
    if (authorName.constructor.name == "Array" || authorName === "") {
        return 'Post:0,comments:0';
    }

    for (var i = 0; i < listOfPosts.length; i++) {

        var obj = listOfPosts[i];
        for (var key in obj) {
            var value = obj[key];

            if (typeof value !== "string") {
                for (var j = 0; j < value.length; j++) {

                    var objinner = value[j];
                    for (var keyinner in objinner) {
                        var valueinner = objinner[keyinner];
                        if (keyinner === "author") {
                            if (authorName === valueinner)
                                countcomments++;
                        }

                    }
                }
            }
            else {
                if (key === "author") {
                    if (authorName === value)
                        countposts++;
                }
            }
        }
    }
    return 'Post:' + countposts + ',comments:' + countcomments;
};

const tickets=(people)=> {
  let originalArray=[]
    for(let x =0 ; x< people.length; x++){
        if(typeof people[x]=='string')
            originalArray.push(Number(people[x]))
        else
            originalArray.push(people[x])
    }
    
    let status = "";
    let value = 0;
    for (let x = 0; x < originalArray.length; x++) {
        if (originalArray[x] - 25 > value) {
            status = "NO";
            break
        } 
        else {
            value += 25;
            status = "YES";
        }
   }
     return status;
    
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
